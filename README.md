
## Prueba tecnica Avanzza.io

Gracias por participar en el proceso de selección para Fullstack Developer Laravel + Vuejs para iniciar con la postulación por favor completa esta prueba técnica las instrucciones se muestran a continuación.
Estas son las indicaciones de la prueba.

1. Crear un API Rest en Laravel que consuman la información de https://football-data.org. Debes crear un usuario (versión free), se asociará un token a tu cuenta.

2. El API debe tener los siguientes endpoints:

- /competitions - Debe obtener el detalle de todas las ligas 
- /competitions/<competition_id> - Obtiene el detalle de una liga específica (Equipos y jugadores que luego serán almacenados localmente)
- /team - Muestra todos los equipos que se han almacenado hasta el momento
- /team/<team_id> - Muestra el detalle de los equipo seleccionado
- /players -  Muestra todos los jugadores almacenados hasta el momento

3. De la información de los jugadores se debe almacenar, nombre, posición número de camiseta

4. Debes asegurarte de enviar las respuestas adecuadas en todos los posibles casos, si se pierde conexión, si no existe el equipo o jugador.

5. Importante: Las consultas a la ApiRest de Football deben ser independientes a las consulta a tu propia ApiRest y considerar que este token free tiene una limitante de 10 request por minuto. Especificar en el README cómo abordaste tu solución.

*SOLUCION:* Se guardo en una base de datos los datos consultados para asi cuando se vuelva a consultar retorne directamente de la base de datos y no sobrecargar el Api de https://football-data.org.

6. Crear una vista de tabla en Vuejs que permita consumir las API creadas (En este punto tienes toda la libertad de agregarle las características que creas relevante para mostrar la información)

7. Debes subirlo a un repositorio y enviarlo hasta el Viernes 08 de Julio

Por favor confirmar la recepción de este correo. Cualquier duda que tengas me puedes escribir.

Saludos
