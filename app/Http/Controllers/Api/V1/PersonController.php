<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Consume\PersonController as ConsumePersonController;
use App\Http\Controllers\Controller;
use App\Models\Player;
use Illuminate\Http\Request;

class PersonController extends Controller
{
    public function get(Request $request)
    {
        $persons = Player::get();
        return response()->json($persons, 200);
    }

    public function show(Request $request, $id)
    {
        $person = Player::find($id);

        if (count($person) === 0) {
            $competitionController = new ConsumePersonController;
            $person = $competitionController->show($id);

            return response()->json($person['data']['teams'], $person['statusCode']);
        }else{
            return response()->json($person, 200);
        }
    }
}
