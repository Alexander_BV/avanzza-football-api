<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Consume\CompetitionController as ConsumeCompetitionController;
use App\Http\Controllers\Controller;
use App\Http\Resources\CompetitionResource;
use App\Models\Area;
use App\Models\Coach;
use App\Models\Competition;
use App\Models\Player;
use App\Models\Team;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Http;

class CompetitionController extends Controller
{
    public function get(Request $request)
    {
        $competitions = Competition::with('area','teams')->get();

        if(count($competitions) === 0){
            $competitionController = new ConsumeCompetitionController;
            $competitions = $competitionController->get();

            return response()->json($competitions['data'], $competitions['statusCode']);
        }else{
            return response()->json($competitions, 200);
        }
    }

    public function show(Request $request, $id)
    {
        $competition = Competition::with('area', 'teams')->where('id', $id)->first();

        if ($competition === null || count($competition->teams) === 0) {
            $competitionController = new ConsumeCompetitionController;
            $competition = $competitionController->show($id);

            return response()->json($competition['data'], $competition['statusCode']);
        }else{
            return response()->json(new CompetitionResource($competition), 200);
        }

    }
}
