<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Consume\TeamController as ConsumeTeamController;
use App\Http\Controllers\Controller;
use App\Models\Team;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TeamController extends Controller
{
    public function get(Request $request)
    {
        $teams = Team::with('area', 'coach', 'players')->get();

        if (count($teams) === 0) {
            $competitionController = new ConsumeTeamController;
            $teams = $competitionController->get();

            return response()->json($teams['data'], $teams['statusCode']);
        }else{
            return response()->json($teams, 200);
        }
    }

    public function show(Request $request, $id)
    {
        $team = Team::with('area', 'coach', 'players')->where('id', $id)->first();

        if ($team === null) {
            $competitionController = new ConsumeTeamController;
            $team = $competitionController->show($id);

            return response()->json($team['data'], $team['statusCode']);
        }else{
            return response()->json($team, 200);
        }   
    }
}
