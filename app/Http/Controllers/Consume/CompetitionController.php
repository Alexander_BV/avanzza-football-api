<?php

namespace App\Http\Controllers\Consume;

use App\Http\Controllers\Controller;

use App\Models\Area;
use App\Models\Coach;
use App\Models\Competition;
use App\Models\Player;
use App\Models\Team;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class CompetitionController extends Controller
{
    public function get()
    {
        try {
            $response = Http::withHeaders([
                'X-Auth-Token'=> env('FOOTBALL_DATA_TOKEN'),
                'Accept-Encoding' => ''
            ])
            ->withOptions(["verify"=>false])
            ->get(env('API_ENDPOINT') . "/v4/competitions");
            $data = $response->json();
    
            if ($response->successful()) {
                foreach ($data['competitions'] as $key => $item) {
    
                    if ($item['area']['id'] !== null) {
                        $area = Area::firstOrCreate(
                            ['id' => $item['area']['id']],
                            [
                                'name' => $item['area']['name'],
                                'code' => $item['area']['code'],
                                'flag' => $item['area']['flag'],
                            ]
                        );
                    }
    
                    $competition = Competition::firstOrCreate(
                        ['id' => $item['id']],
                        [
                            'name' => $item['name'],
                            'code' => $item['code'],
                            'type' => $item['type'],
                            'emblem' => $item['emblem'],
                            'plan'   => $item['plan'],
                            'numberOfAvailableSeasons' => $item['numberOfAvailableSeasons'],
                            'lastUpdated' => $item['lastUpdated'],
                            'areaId' => $item['area']['id'] !== null ? $area->id : null,
                        ]
                    );
                }
    
                return [
                    'success' => true,
                    'statusCode' => 200,
                    'data' => $data['competitions'],
                ];
                // return response()->json($data, 200);
            }else{
                return [
                    'success' => false,
                    'statusCode' => $response->getStatusCode(),
                    'data' => $data,
                ];
                // return response()->json($data, $response->getStatusCode());
            }
        } catch (Exception $e) {
            // return response()->json(["errorCode" => 408, "message" => $e->getMessage()], 408);
            return [
                'success' => false,
                'statusCode' => 408,
                'data' => [
                    "errorCode" => 408,
                    "message" => $e->getMessage()
                ],
            ];
        }
    }

    public function show($id)
    {
        try {
            $response = Http::withHeaders([
                'X-Auth-Token'=> env('FOOTBALL_DATA_TOKEN'),
                'Accept-Encoding' => ''
            ])
            ->withOptions(["verify"=>false])
            ->get(env('API_ENDPOINT') . "/v4/competitions/{$id}/teams");
    
            $data = $response->json();
    
            if ($response->successful()) {
                // - Obtiene el detalle de una liga específica (Equipos y jugadores que luego serán almacenados localmente)
                if($data['competition']['id'] !== null){
                    $competition = Competition::firstOrCreate(
                        ['id' => $data['competition']['id']],
                        [
                            'name' => $data['competition']['name'],
                            'code' => $data['competition']['code'],
                            'type' => $data['competition']['type'],
                            'emblem' => $data['competition']['emblem'],
                        ]
                    );
                }

                foreach ($data['teams'] as $key => $item) {
    
                    if ($item['area']['id'] !== null) {
                        $area = Area::firstOrCreate(
                            ['id' => $item['area']['id']],
                            [
                                'name' => $item['area']['name'],
                                'code' => $item['area']['code'],
                                'flag' => $item['area']['flag'],
                            ]
                        );
                    }
    
                    if($item['coach']['id'] !== null){
                        $coach = Coach::firstOrCreate(
                            ['id' => $item['coach']['id']],
                            [
                                'firstName' => $item['coach']['firstName'],
                                'lastName' => $item['coach']['lastName'],
                                'dateOfBirth' => $item['coach']['dateOfBirth'],
                                'nationality' => $item['coach']['nationality'],
                                'contractStart' => $item['coach']['contract']['start'],
                                'contractUntil' => $item['coach']['contract']['until'],
                            ]
                        );
                    }

                    $team = Team::firstOrCreate(
                        ['id' => $item['id']],
                        [
                            'name' => $item['name'],
                            'shortName' => $item['shortName'],
                            'tla' => $item['tla'],
                            'crest' => $item['crest'],
                            'address' => $item['address'],
                            'website' => $item['website'],
                            'clubColors' => $item['clubColors'],
                            'venue' => $item['venue'],
                            'competitionId' => $data['competition']['id'] !== null ? $data['competition']['id'] : null,
                            'areaId' => $item['area']['id'] !== null ? $item['area']['id'] : null,
                            'coachId' => $item['coach']['id'] !== null ? $item['coach']['id'] : null,
                        ]
                    );
    
                    foreach ($item['squad'] as $key => $value) {
                        $player = Player::firstOrCreate(
                            ['id' => $value['id']],
                            [
                                'name' => $value['name'],
                                'position' => $value['position'],
                                'shirtNumber' => null,
                                'teamId' => $team->id,
                            ]
                        );
                    }
                }
    
                return [
                    'success' => true,
                    'statusCode' => 200,
                    'data' => [
                        'competition' => $data['competition'],
                        'teams' => $data['teams'],
                    ],
                ];
            }else{ 
                return [
                    'success' => false,
                    'statusCode' => $response->getStatusCode(),
                    'data' => $data,
                ];
                // return response()->json($data, $response->getStatusCode());
            }
        } catch (Exception $e) {
            // return response()->json(["errorCode" => 408, "message" => $e->getMessage()], 408);
            return [
                'success' => false,
                'statusCode' => 408,
                'data' => [
                    "errorCode" => 408,
                    "message" => $e->getMessage()
                ],
            ];
        }
       
    }
}
