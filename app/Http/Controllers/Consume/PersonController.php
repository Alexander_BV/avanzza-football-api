<?php

namespace App\Http\Controllers\Consume;

use App\Http\Controllers\Controller;
use App\Models\Player;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PersonController extends Controller
{

    public function show($id)
    {
        try {
            $response = Http::withHeaders([
                'X-Auth-Token'=> env('FOOTBALL_DATA_TOKEN'),
                'Accept-Encoding' => ''
            ])
            ->withOptions(["verify"=>false])
            ->get(env('API_ENDPOINT') . "/v4/persons/{$id}");
            
            $data = $response->json();

            if ($response->successful()) {
                $player = Player::firstOrCreate(
                    ['id' => $data['id']],
                    [
                        'name' => $data['firstName'] . ' ' . $data['lastName'],
                        'position' => $data['position'],
                        'shirtNumber' => $data['shirtNumber'],
                        // 'teamId' => $data['currentTeam']['id'],
                    ]
                );
                return [
                    'success' => true,
                    'statusCode' => 200,
                    'data' => $data,
                ];
                // return response()->json($data, 200);
            }else {
                return [
                    'success' => false,
                    'statusCode' => $response->getStatusCode(),
                    'data' => $data,
                ];
                // return response()->json($data, $response->getStatusCode());
            }
        } catch (Exception $e) {
            return [
                'success' => false,
                'statusCode' => 408,
                'data' => [
                    "errorCode" => 408,
                    "message" => $e->getMessage()
                ],
            ];
            // return response()->json(["errorCode" => 408, "message" => $e->getMessage()], 408);
        }
    }
}
