<?php

namespace App\Http\Controllers\Consume;

use App\Http\Controllers\Controller;
use App\Models\Area;
use App\Models\Coach;
use App\Models\Player;
use App\Models\Team;
use Exception;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Http;

class TeamController extends Controller
{
    public function get()
    {

        try {
            $response = Http::withHeaders([
                'X-Auth-Token'=> env('FOOTBALL_DATA_TOKEN'),
                'Accept-Encoding' => ''
            ])
            ->withOptions(["verify"=>false])
            ->get(env('API_ENDPOINT') . "/v4/teams", ['limit' => '200']);
            
            $data = $response->json();

            if ($response->successful()) {
                foreach ($data['teams'] as $key => $item) {
                    $team = Team::firstOrCreate(
                        ['id' => $item['id']],
                        [
                            'name' => $item['name'],
                            'shortName' => $item['shortName'],
                            'tla' => $item['tla'],
                            'crest' => $item['crest'],
                            'address' => $item['address'],
                            'website' => $item['website'],
                            'clubColors' => $item['clubColors'],
                            'venue' => $item['venue'],
                        ]
                    );
                }
                return [
                    'success' => true,
                    'statusCode' => 200,
                    'data' => $data,
                ];
                // return response()->json($data, 200);
            }else {
                return [
                    'success' => false,
                    'statusCode' => $response->getStatusCode(),
                    'data' => $data,
                ];
                // return response()->json($data, $response->getStatusCode());
            }
        } catch (Exception $e) {
            return [
                'success' => false,
                'statusCode' => 408,
                'data' => [
                    "errorCode" => 408,
                    "message" => $e->getMessage()
                ],
            ];
            // return response()->json(["errorCode" => 408, "message" => $e->getMessage()], 408);
        }
    }

    public function show($id)
    {
        try {

            $response = Http::withHeaders([
                'X-Auth-Token'=> env('FOOTBALL_DATA_TOKEN'),
                'Accept-Encoding' => ''
            ])
            ->withOptions(["verify"=>false])
            ->get(env('API_ENDPOINT') . "/v4/teams/{$id}");
            
            $data = $response->json();

            if ($response->successful()) {
                if ($data['area']['id'] !== null) {
                    $area = Area::firstOrCreate(
                        ['id' => $data['area']['id']],
                        [
                            'name' => $data['area']['name'],
                            'code' => $data['area']['code'],
                            'flag' => $data['area']['flag'],
                        ]
                    );
                }
        
                if($data['coach']['id'] !== null){
                    $coach = Coach::firstOrCreate(
                        ['id' => $data['coach']['id']],
                        [
                            'firstName' => $data['coach']['firstName'],
                            'lastName' => $data['coach']['lastName'],
                            'dateOfBirth' => $data['coach']['dateOfBirth'],
                            'nationality' => $data['coach']['nationality'],
                            'contractStart' => $data['coach']['contract']['start'],
                            'contractUntil' => $data['coach']['contract']['until'],
                        ]
                    );
                }
        
                $team = Team::firstOrCreate(
                    ['id' => $data['id']],
                    [
                        'name' => $data['name'],
                        'shortName' => $data['shortName'],
                        'tla' => $data['tla'],
                        'crest' => $data['crest'],
                        'address' => $data['address'],
                        'website' => $data['website'],
                        'clubColors' => $data['clubColors'],
                        'venue' => $data['venue'],
                        'areaId' => $data['area']['id'] !== null ? $area->id : null,
                        'coachId' => $data['coach']['id'] !== null ? $coach->id : null,
                    ]
                );
        
                foreach ($data['squad'] as $key => $value) {
                    $player = Player::firstOrCreate(
                        ['id' => $value['id']],
                        [
                            'name' => $value['name'],
                            'position' => $value['position'],
                            'shirtNumber' => null,
                            'teamId' => $team->id,
                        ]
                    );
                }

                return [
                    'success' => true,
                    'statusCode' => 200,
                    'data' => $data,
                ];
                // return response()->json($data, 200);
            }else {
                return [
                    'success' => false,
                    'statusCode' => $response->getStatusCode(),
                    'data' => $data,
                ];
                // return response()->json($data, $response->getStatusCode());
            }
        } catch (Exception $e) {
            return [
                'success' => false,
                'statusCode' => 408,
                'data' => [
                    "errorCode" => 408,
                    "message" => $e->getMessage()
                ],
            ];
            // return response()->json(["errorCode" => 408, "message" => $e->getMessage()], 408);
        }

    }
}
