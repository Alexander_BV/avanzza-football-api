<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CompetitionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'competition' => [
                'id' => $this->id,
                'name' => $this->name,
                'code' => $this->code,
                'type' => $this->type,
                'emblem' => $this->emblem,
                'plan' => $this->plan,
                'numberOfAvailableSeasons' => $this->numberOfAvailableSeasons,
                'lastUpdated' => $this->lastUpdated,
            ],
            'teams' => $this->teams,
            'area' => $this->area,
        ];
    }
}
