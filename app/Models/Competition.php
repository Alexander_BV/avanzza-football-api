<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    use HasFactory;

    public $incrementing = false;
    protected $fillable = ['id', 'name', 'code', 'type', 'emblem', 'plan', 'numberOfAvailableSeasons', 'lastUpdated', 'areaId'];
    protected $hidden = ['created_at', 'updated_at'];
    // protected $appends = ['competition'];

    // public function getCompetitionAttribute()
    // {
    //     return [
    //         'name' => $this->name,
    //         'code' => $this->code,
    //     ];
    // }

    public function area()
    {
        return $this->belongsTo(Area::class, 'areaId');
    }

    public function teams()
    {
        return $this->hasMany(Team::class, 'competitionId');

    }
}
