<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coach extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $fillable = ['id', 'firstName', 'lastName', 'dateOfBirth', 'nationality', 'contractStart', 'contractUntil'];
    protected $appends = ['name', 'contract'];
    protected $hidden = ['contractStart', 'contractUntil', 'created_at', 'updated_at'];

    public function getNameAttribute()
    {
        return "{$this->firstName} {$this->lastName}";
    }

    public function getContractAttribute()
    {
        return [
            'start' => $this->contractStart,
            'until' => $this->contractUntil,
        ];
    }
}
