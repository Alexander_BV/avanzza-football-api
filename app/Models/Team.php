<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory;
    public $incrementing = false;
    protected $fillable = ['id', 'name', 'shortName', 'tla', 'crest', 'address', 'website', 'clubColors', 'venue', 'areaId', 'coachId', 'competitionId'];
    protected $hidden = ['created_at', 'updated_at'];

    public function area()
    {
        return $this->belongsTo(Area::class, 'areaId');
    }

    public function coach()
    {
        return $this->belongsTo(Coach::class, 'coachId');
    }

    public function players()
    {
        return $this->hasMany(Player::class, 'teamId');
    }
}
