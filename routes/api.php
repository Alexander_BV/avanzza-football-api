<?php

use App\Http\Controllers\Api\V1\CompetitionController as ApiCompetitionController;
use App\Http\Controllers\Api\V1\TeamController as ApiTeamController;
use App\Http\Controllers\Api\V1\PersonController as ApiPersonController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('v1')->group(function () {
    Route::prefix('competitions')->group(function () {
        Route::get('/', [ApiCompetitionController::class, 'get']);
        Route::get('/{id}', [ApiCompetitionController::class, 'show']);
    });
    Route::prefix('teams')->group(function () {
        Route::get('/', [ApiTeamController::class, 'get']);
        Route::get('/{id}', [ApiTeamController::class, 'show']);
    });
    Route::prefix('persons')->group(function () {
        Route::get('/', [ApiPersonController::class, 'get']);
        Route::get('/{id}', [ApiPersonController::class, 'show']);
    });
});

