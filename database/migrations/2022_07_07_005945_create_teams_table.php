<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teams', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->primary('id');
            $table->string('name');
            $table->string('shortName')->nullable();
            $table->string('tla')->nullable();
            $table->string('crest')->nullable();
            $table->string('address')->nullable();
            $table->string('website')->nullable();
            // $table->string('founded');
            $table->string('clubColors')->nullable();
            $table->string('venue')->nullable();

            $table->integer('competitionId')->unsigned()->nullable();
            $table->foreign('competitionId')->references('id')->on('competitions');

            $table->integer('areaId')->unsigned()->nullable();
            $table->foreign('areaId')->references('id')->on('areas');

            $table->integer('coachId')->unsigned()->nullable();
            $table->foreign('coachId')->references('id')->on('coaches');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teams');
    }
}
