<!DOCTYPE html>
<html>

<head>
    @include('sections.head')
</head>

<body>
    
    <!-- Sidenav -->
    @include('sections.sidenav')
    
    <!-- Main content -->
    <div class="main-content" id="app">
        <!-- Topnav -->
        @include('sections.topnav')
        
        <!-- Header -->
        <div class="header bg-success pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    @yield('header-body') 
                </div>
            </div>
        </div>

        <!-- Page content -->
        <div class="container-fluid mt--6">

            @yield('content-main')

        </div>
    </div>

    <!-- ROLES SPATIE -->
    <script>
        @auth
            window.Roles = {!! json_encode(Auth::user()->allRoles, true) !!};
            window.Permissions = {!! json_encode(Auth::user()->allPermissions, true) !!};
        @else
            window.Roles = [];
            window.Permissions = [];
        @endauth
    </script>
   
    <!-- Argon Scripts -->
    @include('sections.scripts')
    
</body>

</html>
