@extends('layout.app')

@section('extra-css')
@endsection

@section('header-body')
@endsection

@section('content-main')
    <competitions-list></competitions-list>
@endsection

@section('extra-js')
    <script src="{{ asset('js/app.js') }}"></script>    
@endsection