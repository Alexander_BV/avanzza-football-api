<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<meta name="description" content="POS Restaurant, atencion de clientes">
<meta name="author" content="Alexander BV">

<meta name="asset-url" content="{{ config('app.asset_url') }}">
<!-- <meta name="asset-url" content="http://192.168.1.101:8001"> -->

<title> @yield('title-header', 'SISTEMA RESTAURANTE') </title>

<!-- Favicon -->
<link rel="icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">
<!-- Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">

<!-- Icons -->
{{-- <link rel="stylesheet" href="{{ asset('assets/vendor/nucleo/css/nucleo.css') }}" type="text/css"> --}}
<link rel="stylesheet" href="{{ asset('assets/vendor/@fortawesome/fontawesome-free/css/all.min.css') }}" type="text/css">

<!--  SWEETALERT -->
{{-- <link rel="stylesheet" href="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.css') }}">  --}}

<!-- Argon CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/argon.css') }}"  type="text/css">

<style>

.form-group.is-error > .form-control-label{
    color: #fb6340 !important;
}

.form-group.is-error > input{
    border-color: #fb6340 !important
}

.form-group.is-error > .input-group > .vs--searchable > .vs__dropdown-toggle{
    /* border-color: # !important */
    border: 1.1px solid #fb6340;
}

.form-group.is-error > .invalid-feedback{
    display: block;
}

.btn-swal{
    font-size: 1rem;
}

body.modal-open {
    overflow: hidden;
}

.noselect {
    user-select: none; /* Non-prefixed version, currently supported by Chrome, Edge, Opera and Firefox */
    -webkit-touch-callout: none; /* iOS Safari */
    -webkit-user-select: none; /* Safari */
    -khtml-user-select: none; /* Konqueror HTML */
    -moz-user-select: none; /* Old versions of Firefox */
    -ms-user-select: none; /* Internet Explorer/Edge */
}

.no-drag{
    user-drag: none; 
    user-select: none;
    -moz-user-select: none;
    -webkit-user-drag: none;
    -webkit-user-select: none;
    -ms-user-select: none;
}

</style>



@yield('open-graph')

@yield('extra-css')