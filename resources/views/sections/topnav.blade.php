<nav class="navbar navbar-top navbar-expand navbar-dark bg-success border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
           
            <!-- Navbar links -->
            <ul class="navbar-nav align-items-center  ml-md-auto ">
                <li class="nav-item d-xl-none">
                    <!-- Sidenav toggler -->
                    <div class="pr-3 sidenav-toggler sidenav-toggler-dark" data-action="sidenav-pin"
                        data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav align-items-center  ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                        aria-expanded="false">
                        <div class="media align-items-center">
                            <span class="avatar avatar-sm rounded-circle">
                                <svg aria-hidden="true" class="c013" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" width="64" height="64"><linearGradient id="g2" gradientUnits="userSpaceOnUse" x1="2.8318" y1="16.25" x2="17.1682" y2="16.25"><stop offset="0.3" stop-color="#B2AEAA"></stop><stop offset="0.8" stop-color="#33312D"></stop></linearGradient><linearGradient id="g1" gradientUnits="userSpaceOnUse" x1="6" y1="7.5" x2="14" y2="7.5"><stop offset="0.15" stop-color="#B2AEAA"></stop><stop offset="1" stop-color="#393632"></stop></linearGradient><circle fill="#33312D" cx="10" cy="10" r="10"></circle><path fill="url(#g2)" d="M10,20c2.8,0,5.4-1.2,7.2-3c-1.3-2.6-4-4.5-7.2-4.5S4.1,14.3,2.8,17C4.6,18.8,7.2,20,10,20z"></path><circle fill="url(#g1)" cx="10" cy="7.5" r="4"></circle></svg>
                            </span>
                            <div class="media-body ml-2 d-none d-lg-block">
                                <span class="mb-0 text-sm font-weight-bold">
                                    Mi cuenta
                                </span>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>
