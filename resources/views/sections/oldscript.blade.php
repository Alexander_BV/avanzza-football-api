<!-- Core -->
<script src="{{ asset('assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/vendor/js-cookie/js.cookie.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') }}"></script>

<!-- Optional JS -->
<script src="{{ asset('assets/vendor/dropzone/dist/min/dropzone.min.js') }}"></script>

<!-- Argon JS -->
<script src="{{ asset('assets/js/argon.js') }}"></script>
<!--  SELECT2 -->
{{-- <script src="{{ asset('assets/vendor/select2/dist/js/select2.min.js') }}"></script> --}}

 <!--  SWEETALERT -->
<script src="{{ asset('assets/vendor/sweetalert2/dist/sweetalert2.min.js') }}"></script> 

<!-- NOTIFICACIONES -->
<script src="{{ asset('assets/vendor/bootstrap-notify/bootstrap-notify.min.js') }}"></script>

<!-- Demo JS - remove this in your project -->
{{-- <script src="{{ asset('assets/js/demo.min.js') }}"></script> --}}

{{-- <script src="{{ asset('assets/vendor/loading/loading_overlay.js') }}"></script> --}}

<script>
    $(function () {
      $('[data-toggle="tooltip"]').tooltip()
    })
</script>


@yield('extra-js')