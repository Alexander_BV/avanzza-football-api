<nav class="sidenav navbar navbar-vertical fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
  <div class="scrollbar-inner">

    <div class="sidenav-header  d-flex  align-items-center">
      <a class="navbar-brand" href="/">
        <img src="{{ asset('assets/img/logo_avanzza.png') }}" class="navbar-brand-img" alt="Logo App" style="max-height: 3rem;">
      </a>
      <div class=" ml-auto ">
        <!-- Sidenav toggler -->
        <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
          <div class="sidenav-toggler-inner">
            <i class="sidenav-toggler-line"></i>
            <i class="sidenav-toggler-line"></i>
            <i class="sidenav-toggler-line"></i>
          </div>
        </div>
      </div>
    </div>

    <div class="navbar-inner">
      <div class="collapse navbar-collapse" id="sidenav-collapse-main">
        <ul class="navbar-nav">

          <li class="nav-item">
            <a class="nav-link" href="#">
              <i class="fas fa-store-alt text-success"></i>
              <span class="nav-link-text">Competiciones</span>
            </a>
          </li>

        </ul>
      </div>
    </div>

  </div>
</nav>
