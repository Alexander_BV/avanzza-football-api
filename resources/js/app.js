require('./bootstrap');

window.Vue = require('vue');

import 'vue-loading-overlay/dist/vue-loading.css';

import Vue  from 'vue'
Vue.component('loading-overlay', require('vue-loading-overlay/src/js/Component.vue').default);

Vue.component('competitions-list', require('./components/CompetitionsList.vue').default);

const app = new Vue({
    el: '#app',
    // store: store
});

